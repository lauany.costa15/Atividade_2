namespace exemplos_condicoes
{
    let idade: number = 19; 

    if (idade >= 18)
    {//inicio do bloco 
        console.log("Pode Dirigir");
    }//fim do bloco
    else
    {
        console.log("Não pode dirigir"); 
    }

    //Ternario
    idade >= 18 ? console.log("Pode dirigir") : console.log("Não pode dirigir"); 

}
