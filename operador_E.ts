//Testando o operador E - && 
namespace operador_E
{
    let idade = 16;
    let MaiorIdade = idade > 18;
    let PossuiCarteiraDeMotorista = false;
    
    let PodeDirigir = MaiorIdade && PossuiCarteiraDeMotorista;
    console.log( PodeDirigir ); // false
}