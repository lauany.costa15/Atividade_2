//A nota final de um estudante é calculada a partir de três notas atribuídas respectivamente a um trabalho de laboratório, a uma avaliação semestral e a um exame final. Faça um programa que receba as três notas, calcule e mostre a média ponderada e conceito.

namespace exercicio_2 
{
    let Nota1, Peso1, Nota2, Peso2, Nota3, Peso3: number; 
    Nota1 = 1; 
    Nota2 = 1,5;
    Nota3 = 3; 
    Peso1 = 2;
    Peso2 = 3;
    Peso3 = 5;
    
    let MediaPonderada: number; 
    MediaPonderada = (Nota1 * Peso1 + Nota2 * Nota2 + Nota3 * Peso3)/ (Peso1 + Peso2 + Peso3);

    if (MediaPonderada >=8 && MediaPonderada <10)
{
        console.log ("A");
}

    if (MediaPonderada >=7 && MediaPonderada <8)
{
        console.log ("B");
}

    if (MediaPonderada >=6 && MediaPonderada <7)
{
        console.log ("C");
}

if (MediaPonderada >=5 && MediaPonderada <6)
{
        console.log("D");
}
    if (MediaPonderada >=0 && MediaPonderada <5)
{
        console.log ("E"); 
}
}
